using UnityEngine;
using UnityEngine.AI;

public class LevelManager : GenericSingletonClass<LevelManager>
{
    public Ball Ball;
    public Attacker Attacker;
    public Transform Goal;
    public Transform ShootPos;
    public Transform Spawn;

    private NavMeshAgent m_BallOwner;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetBallOwner(NavMeshAgent agent)
    {
        m_BallOwner = agent;
    }

    public NavMeshAgent GetBallOwner()
    {
        return m_BallOwner;
    }
}
