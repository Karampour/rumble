using UnityEngine;
using UnityEngine.AI;

public class Goaler : MonoBehaviour
{
    private NavMeshAgent m_Agent;

    private Plane m_Plane;
    private Rigidbody m_Ballrb;

    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Plane = new Plane(transform.forward, transform.position);
    }

    // Start is called before the first frame update
    void Start()
    {
        m_Ballrb = LevelManager.Instance.Ball.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.Instance.GetBallOwner() == null)
        {
            if (m_Ballrb != null)
            {
                Vector3 dir = Vector3.Normalize(m_Ballrb.velocity);
                Ray ray = new Ray(m_Ballrb.position, dir);
                if (m_Plane.Raycast(ray, out float dis))
                {
                    Vector3 pos = ray.GetPoint(dis);

                    float rnd = Random.Range(-6f, 6f);
                    pos.z += rnd;

                    m_Agent.SetDestination(pos);
                }
            }
        }
    }
}
