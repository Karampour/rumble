using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;


public class Attacker : MonoBehaviour
{
    [SerializeField] private BallSocket m_BallSocket;

    private NavMeshAgent m_Agent;
    private bool m_IsOwner = false;
    private bool m_GoingForShoot = false;

    private CancellationTokenSource m_Token = new CancellationTokenSource();
    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
    }


    private void OnOwnerChange(NavMeshAgent agent)
    {
        if (agent == m_Agent)
        {
            m_IsOwner = true;
        }
        else
        {
            m_IsOwner = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                m_Agent.SetDestination(hit.point);
            }
        }

        if (!m_IsOwner)
        {
            if (LevelManager.Instance.GetBallOwner() != null) return;

            Vector3 ballPos = LevelManager.Instance.Ball.transform.position;

            m_Agent.SetDestination(ballPos);
        }
        else
        {

            if (!m_GoingForShoot)
            {
                float rnd = Random.Range(-3, 3);
                //print(rnd);
                Vector3 shootPos = LevelManager.Instance.ShootPos.position;

                Vector3 pos = new Vector3(shootPos.x, shootPos.y, shootPos.z + rnd);
                m_Agent.SetDestination(pos);
                Shoot(pos, m_Token.Token);
            }
            m_GoingForShoot = true;


        }
    }

    public void SetOwner(bool state)
    {
        m_IsOwner = state;
    }

    private async void Shoot(Vector3 from, CancellationToken token)
    {
        if (token.IsCancellationRequested) return;

        while (Vector3.Distance(transform.position, from) > 3f)
        {
            //print(Vector3.Distance(transform.position, from));
            await Task.Yield();
            if (token.IsCancellationRequested) return;
        }

        float rnd = Random.Range(-4.0f, 4.0f);
        Vector3 goal = LevelManager.Instance.Goal.transform.position;
        Vector3 target = new Vector3(goal.x, goal.y, goal.z + rnd);

        //print(target);
        Debug.DrawRay(transform.position, target - transform.position, Color.red, 2);

        Vector3 dir = Vector3.Normalize(target - transform.position);
        Vector3 shootPos = transform.position + dir * 6f;
        m_Agent.SetDestination(shootPos);

        while (Vector3.Distance(transform.position, shootPos) > 1.3f && !token.IsCancellationRequested)
        {
            //print(Vector3.Distance(transform.position, shootPos));
            await Task.Yield();
            if (token.IsCancellationRequested) return;
        }

        await Task.Delay(1000);
        m_BallSocket.Shoot(target, 40);
        m_GoingForShoot = false;

        if (token.IsCancellationRequested) return;
    }

    private void OnDestroy()
    {
        m_Token.Cancel();
    }
}
