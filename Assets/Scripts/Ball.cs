using UnityEngine;

public class Ball : MonoBehaviour
{
    [Range(0f, 10f)]
    public float AttractionForce;
    private Rigidbody rb;

    private bool m_IsOwned = false;
    private bool m_ShouldAttract = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        Attacker attacker = other.GetComponent<Attacker>();
        if (attacker != null)
        {
            if (m_ShouldAttract)
            {
                Vector3 force = (attacker.transform.position - rb.transform.position) * AttractionForce;
                rb.AddForce(force);
            }
        }
    }


    public void Own(Transform parent)
    {
        rb.isKinematic = true;
        rb.transform.position = parent.position;
        rb.transform.SetParent(parent);
        m_IsOwned = true;
    }

    public void Disown()
    {
        rb.isKinematic = false;
        rb.transform.SetParent(null);
        m_IsOwned = false;
        LevelManager.Instance.SetBallOwner(null);
    }

    public void Shoot(Vector3 target, float force)
    {
        if (!m_IsOwned) return;

        Disown();
        Vector3 dir = Vector3.Normalize(target - rb.position);

        rb.AddForce(dir * force, ForceMode.Impulse);
        //m_ShouldAttract = false;
    }

    public void Spawn()
    {
        float rndX = Random.Range(-5f, 5f);
        float rndZ = Random.Range(-5f, 5f);

        Vector3 Spawn = LevelManager.Instance.Spawn.position;
        Spawn.x += rndX;
        Spawn.z += rndZ;

        rb.isKinematic = true;

        rb.position = Spawn;

        rb.isKinematic = false;
    }

}
