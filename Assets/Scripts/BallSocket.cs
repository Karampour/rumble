using System.Threading.Tasks;
using UnityEngine;

public class BallSocket : MonoBehaviour
{
    [SerializeField] private Attacker m_Attacker;
    private Ball m_Ball;


    private bool m_ShouldCatch = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (m_Ball != null)
            {
                DeactiveSocket(300);
                m_Ball.Shoot(transform.position + transform.forward, 20);
                m_Ball = null;
                m_Attacker.SetOwner(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Ball ball = other.GetComponent<Ball>();
        if (ball != null)
        {
            if (m_ShouldCatch)
            {
                ball.Own(transform);
                m_Ball = ball;
                m_Attacker.SetOwner(true);
            }
        }
    }

    public async void DeactiveSocket(int cooldown)
    {
        m_ShouldCatch = false;
        await Task.Delay(cooldown);
        m_ShouldCatch = true;
    }

    public void Shoot(Vector3 target, float force)
    {
        DeactiveSocket(300);
        m_Ball.Shoot(target, force);
        m_Ball = null;
        m_Attacker.SetOwner(false);
    }
}
